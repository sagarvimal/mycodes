package test.designpattern.observer;

import java.util.Scanner;

public class ObserverMain {
	
	public static void main(String[] args) {
		
		Scanner scn = null;
		try{
		DecimalInput decimalInput = new DecimalInput();
		
		new BinaryObserver(decimalInput);
		new HexObserver(decimalInput);
		
		System.out.println("Enter Decimal number : ");
		scn = new Scanner(System.in);
		
		int input = scn.nextInt();
		
		decimalInput.setDecimal(input);
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			if(scn != null)
			{
				scn.close();
			}
		}
	}
}
