package test.designpattern.observer;

public class BinaryObserver implements IObserver{
	
	public BinaryObserver(DecimalInput decimalInput) {

		decimalInput.registerObserver(this);
	}

	@Override
	public void update(DecimalInput decimalInput) {
		
		System.out.println("Binary : "+ Integer.toBinaryString(decimalInput.getDecimal()));
	}
}
