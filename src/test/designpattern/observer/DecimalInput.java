package test.designpattern.observer;

import java.util.ArrayList;
import java.util.List;

public class DecimalInput {
	
	private int decimal;
	
	List<IObserver> iObservers = new ArrayList<IObserver>(); 
	
	public void setDecimal(int decimal)
	{
		this.decimal = decimal;	
		
		System.out.println("Decimal value set to : "+ this.decimal);
		
		// notify all observers about the change
		this.notifyObservers();
	}
	
	public int getDecimal()
	{
		return this.decimal;
	}
	
	public void registerObserver(IObserver iObserver)
	{
		iObservers.add(iObserver);
	}
	
	private void notifyObservers()
	{
		for(int i=0; i< iObservers.size(); i++)
		{
			iObservers.get(i).update(this);
		}
	}
}
