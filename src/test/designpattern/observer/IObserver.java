package test.designpattern.observer;

public interface IObserver {
	
	void update(DecimalInput decimalInput);
}
