package test.designpattern.observer;

public class HexObserver implements IObserver{

	@Override
	public void update(DecimalInput decimalInput) {

		System.out.println("Hex Value test: "+ Integer.toHexString(decimalInput.getDecimal()));
	}
	
	public HexObserver(DecimalInput decimalInput) {
		
		decimalInput.registerObserver(this);
	}
}
